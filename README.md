# Automated setup

## Complete setup

### Ubuntu

-   git
-   yadm
-   zsh
-   [my dotfiles](https://gitlab.com/david-plugge/dotfiles.git)

```bash
curl -o- https://gitlab.com/david-plugge/setup/raw/main/scripts/install-ubuntu.sh | sh
```

## Scripts

### Docker

```bash
curl -o- https://gitlab.com/david-plugge/setup/raw/main/scripts/install-docker.sh | sh
```

### UFW

```bash
curl -o- https://gitlab.com/david-plugge/setup/raw/main/scripts/setup-ufw.sh | sh
```

## use wget

replace `curl -o-` with `wget -qO-`
