#!/bin/sh

# update repos
sudo apt-get update
# install dependencies
sudo apt install -y git yadm zsh wget unzip

# load dotfiles
yadm clone https://gitlab.com/david-plugge/dotfiles.git --bootstrap

# change default shell to zsh
sudo chsh -s $(which zsh) $(whoami)
