# configure firewall (ufw)
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https

echo
echo "run 'sudo ufw enable' to enable ufw"
